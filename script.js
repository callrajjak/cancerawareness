var app = angular.module('cancerUI', ['ui.router', 'firebase', 'chart.js']);

app.config(function ($stateProvider, $locationProvider, $urlMatcherFactoryProvider, $urlRouterProvider, ChartJsProvider) {

  $urlMatcherFactoryProvider.caseInsensitive(true);
  $urlRouterProvider.otherwise('home');
  ChartJsProvider.setOptions({
    colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
    //colors: ['#00bfff', '#1e90ff', '#ff6eb4', '#ff6a6a', '#b2dfee', '#ffaeb9','#87cefa']
  });

  $stateProvider
    .state("home", {
      url: "/",
      views: {
        headerContent: {
          templateUrl: "templates/header.html"
        },
        mainContent: {
          templateUrl: "templates/home.html",
          controller: "homeController",
          controllerAs: "homeCtrl"
        },
        footerContent: {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state("aboutcontact", {
      url: "/aboutcontact",
      views: {
        headerContent: {
          templateUrl: "templates/header.html"
        },
        mainContent: {
          templateUrl:"templates/aboutus.html",
        },
        footerContent: {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state("admin_dashboard", {
      url: "/admin/dashboard",
      views: {
        headerContent: {
          templateUrl: "templates/admin/header.html",
          controller: "admindashController",
          controllerAs: "admindashCtrl"
        },
        mainContent: {
          templateUrl: "templates/admin/dashboard.html",
          controller: "admindashController",
          controllerAs: "admindashCtrl",
          resolve: {
            check: function($state, userService) {
              if (!userService.isUserLoggedIn()) {
                $state.go("home");
              }
            }
          }
        },
        footerContent: {
          templateUrl: "templates/admin/footer.html"
        }
      }
    })
    .state("tester_dashboard", {
      url: "/tester/dashboard",
      views: {
        headerContent: {
          templateUrl: "templates/tester/header.html",
          controller: "testerdashController",
          controllerAs: "testerdashCtrl"
        },
        mainContent: {
          templateUrl: "templates/tester/dashboard.html",
          controller: "testerdashController",
          controllerAs: "testerdashCtrl",
          resolve: {
            check: function($state, userService) {
              if (!userService.isUserLoggedIn()) {
                $state.go("home");
              }
            }
          }
        },
        footerContent: {
          templateUrl: "templates/tester/footer.html"
        }
      }
    })
    .state("tester_dashboard.test", {
      url: "/",
      views: {
        testerContent: {
          templateUrl: "templates/tester/test-forms.html",
          controller: "testController",
          controllerAs: "testCtrl"
        }
      }
    })
    .state("tester_dashboard.summary", {
      url: "/",
      views: {
        testerContent: {
          templateUrl: "templates/tester/tester-summary.html",
          controller: "testerdashController",
          controllerAs: "testerdashCtrl"
        }
      }
    })
    .state("tester_dashboard.result", {
      url: "/",
      views: {
        testerContent: {
          templateUrl: "templates/tester/result.html",
          controller: "resultController",
          controllerAs: "resCtrl"
        }
      }
    })
    .state("404", { url: "/404", templateUrl: "404.html" });
  $locationProvider.html5Mode(true);
});

app.controller('homeController', function ($state, $location) {

    this.Title = "Cancer Prediction System";

  })
  .controller('testerdashController', function ($state, $http, userService, testerDetails) {
    viewModel = this;
    viewModel.username = userService.getName();


    viewModel.logoutModal = function () {
      userService.userLoggedOut();
      $state.go('home');
    };

    config = testerDetails.configDetails();
    var currentUserDetails = config.authRef.currentUser;
    viewModel.count = 0;
    if (currentUserDetails) {
      config.userRef.child(currentUserDetails.uid).child('result').on('value', function (data) {
        viewModel.userData = data.val();
        angular.forEach(viewModel.userData, function (value) {
          viewModel.count++;
        });
      });
    }

  })
  .controller('resultController', function ($state, $http, userService, testerDetails) {
    var risksum = 0,
      symptomsum = 0,
      riskcount = 0,
      symptomcount = 0,
      riskvalue = '',
      symptomvalue = '',
      organ = '',
      gender = '';

    var ansMetrix = [
      {
        riskavg: 'High',
        symptomavg: 'High',
        answer: 'High'
      },
      {
        riskavg: 'High',
        symptomavg: 'Medium',
        answer: 'High'
      },
      {
        riskavg: 'High',
        symptomavg: 'Low',
        answer: 'High'
      },
      {
        riskavg: 'Medium',
        symptomavg: 'High',
        answer: 'High'
      },
      {
        riskavg: 'Medium',
        symptomavg: 'Medium',
        answer: 'Medium'
      },
      {
        riskavg: 'Medium',
        symptomavg: 'Low',
        answer: 'Low'
      },
      {
        riskavg: 'Low',
        symptomavg: 'High',
        answer: 'Medium'
      },
      {
        riskavg: 'Low',
        symptomavg: 'Medium',
        answer: 'Medium'
      },
      {
        riskavg: 'Low',
        symptomavg: 'Low',
        answer: 'Low'
      },
    ];
    var levelValue = ['Low', 'Medium', 'High', 'High'];
    var percentageValues = [{
      min: '10',
      max: '40'
    }, {
      min: '40',
      max: '60'
    }, {
      min: '70',
      max: '100'
    }, {
      min: '70',
      max: '100'
    }, ];
    
    var organTests = [
      {
        type: 'Oral',
        tests: 'biopsy of tongue and inner mouth'
      },
      {
        type: 'Stomach',
        tests: 'endoscopy of stomach'
      },
      {
        type: 'Breast',
        tests: 'mammogram and PET scan of breast'
      },
      {
        type: 'Prostate',
        tests: 'Prostate - specific antigen(PSA) test'
      },
      {
        type: 'Ovarian',
        tests: 'Transvaginal ultrasound (TVUS)'
      },
      {
        type: 'Lung',
        tests: 'X - ray, Or CT scan, or MRI'
      },
      {
        type: 'Brain',
        tests: ''
      }
    ];

     
    this.final = 'None';
    
    var finalvalue = '';

    this.riskFactorsFinal = userService.getRiskValues();
    this.username = userService.getName();
    
    var riskvalues = Object.values(this.riskFactorsFinal);
    angular.forEach(riskvalues, function (value) {
      angular.forEach(value, function (realvalue, key) {
        if (key != 'symptoms') {
          if (angular.isNumber(realvalue)) {
            risksum = risksum + realvalue;
            riskcount++;
          } else {
            if (value.hasOwnProperty('gender')) {
              userService.setGender(realvalue);

            }
          }
        } else {
          if (angular.isNumber(realvalue)) {
            symptomsum = symptomsum + realvalue;
            symptomcount++;
          }
        }
      });
    });

    riskavg = Math.round(risksum / riskcount);
    symptomavg = Math.round(symptomsum / symptomcount);

    if (riskavg != 0) {
      riskvalue = levelValue[riskavg - 1];
    } else {
      riskvalue = levelValue[riskavg];
    }
    if (symptomavg != 0) {
      symptomvalue = levelValue[symptomavg - 1];
    } else {
      symptomvalue = levelValue[symptomavg];
    }
    angular.forEach(ansMetrix, function (metrixvalue) {
      if (metrixvalue.riskavg == riskvalue && metrixvalue.symptomavg == symptomvalue) {
        finalvalue = metrixvalue.answer;
      }
    });
    /* var index1 = levelValue.findIndex(finalvalue); */
    var index = levelValue.indexOf(finalvalue);
    /* if(index) { */
      max = parseInt(percentageValues[index].max);
      min = parseInt(percentageValues[index].min);
      this.percentage = Math.floor(Math.random() * (max - min + 1) + min);
    /* } */

   gender = userService.getGender();
   this.Gender = gender;
   organ = userService.getOrgan();
   this.Organ = organ;

   for (var key in organTests) {
     if (organTests.hasOwnProperty(key)) {
       var element = organTests[key];
       if (element.type === this.Organ) {
         this.organTests = element.tests;
       }
     }
   }

    this.final = finalvalue;
    setResultDetailsToFirebase(finalvalue);

    function setResultDetailsToFirebase(finalvalue) {
      config = testerDetails.configDetails();
      var currentUserDetails = config.authRef.currentUser;
      resultdate = Date.now();
      var userObj = {
        testresult: finalvalue,
        testorgan: organ
      };
      if (currentUserDetails) {
        config.userRef.child(currentUserDetails.uid).update({
          gender: gender
        });
        config.userRef.child(currentUserDetails.uid)
          .child('result').child(resultdate).set(userObj);
        $state.go('tester_dashboard.result');
      }
    }
    this.printDoc = function (printableDoc) {
      var innerContents = document.getElementById(printableDoc).innerHTML;
      var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
      popupWinindow.document.open();
      popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
      popupWinindow.document.close();
    };

    userService.clearRiskValues();

  })
  .controller('admindashController', function ($scope, $state, $http, userService, testerDetails) {
    this.username = userService.getName();
    $scope.labels = [];
    maleOragansCount = [];
    femaleOragansCount = [];

    $scope.labels = userService.getOrganList();
    /* angular.forEach(symptoms, function (symptomlist) {
      angular.forEach(symptomlist.possibilities, function (possiblelist) {
        $scope.labels.push(possiblelist.answer);
      });
    }); */

    //$scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    $scope.series = ['Male', 'Female'];

    var males = [];
    var females = [];
    config = testerDetails.configDetails();
    maleOrganDetails = testerDetails.getMaleRatioDetails();
    femaleOrganDetails = testerDetails.getFemaleRatioDetails();
   
    $scope.malearray = userService.getMalesArray();
    if($scope.malearray.length != 0)
    {
      males = userService.getArrays('associate','maleorgancount',$scope.labels);
      $scope.malesCount = males;
    }
    
    $scope.femalearray = userService.getFemalesArray();
    if($scope.femalearray.length != 0){
      females = userService.getArrays('associate', 'femaleorgancount',$scope.labels);
      $scope.femalesCount = females;
    }

    $scope.data = [
      males,females
    ];

    $scope.doughnutlabels = ["Low", "Medium","High"];
    $scope.doughnutdata = [30, 50, 80];
    var lowcount = 0,mediumcount = 0,highcount = 0;
    config.userRef.on('value',function(data){
      angular.forEach(data.val(),function(datavalue){
        angular.forEach(datavalue.result,function(resultValue){
          if(resultValue.testresult == 'Low')
          {
            lowcount++;
          }
          else if(resultValue.testresult == 'Medium')
          {
            mediumcount++;
          }
          else if(resultValue.testresult == 'High')
          {
            highcount++;
          }
        });
      });
    });
    
    $scope.doughnutdata = [
      userService.getResultAverage().low,
      userService.getResultAverage().medium,
      userService.getResultAverage().high
    ];

    $scope.pielabels = ["Males", "Females"];
    $scope.piedata = [
      userService.getTotalGender().males,userService.getTotalGender().females
    ];


    $scope.horizontalbarlabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    $scope.horizontalbarseries = ['Males', 'Females'];

    $scope.horizontalbardata = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ];
    this.logoutModal = function(){
      config.authRef.signOut().then(function(){
        $state.go('home');
      }).catch(function(error){
        userService.showAlert({
          title: error.code,
          details: error.message,
          className: 'text-danger'
        });
      });
    };
  })
  .controller('testController', function ($scope, $state, $http, userService, DataService) {
    viewModel = this;
    viewModel.username = userService.getName();
    viewModel.tab = 1;
    viewModel.stepForm = 'Personal Information';
    viewModel.organtab = 'Lung';
    viewModel.riskFactors = [];
    viewModel.symptoms = [];
    viewModel.finalRisks = [];
    viewModel.percentage = {
      'width': '20%'
    };
    viewModel.initializeInformation = function () {
      if (viewModel.riskFactors.length == 0 && viewModel.symptoms.length == 0) {
        viewModel.riskFactors = userService.generateRiskFactorsList();
        viewModel.symptoms = userService.generateSymptomsList();
      }
    };

    viewModel.getRadioValue = function (group, value) {
      angular.forEach(viewModel.riskFactors, function (list) {
        if (list.type == 'radio' && list.group == group) {
          angular.forEach(list.selected, function (selectlist) {
            if (selectlist.answer == value) {
              selectlist.value = true;
              list.final = selectlist.answervalue;
              if(selectlist.answervalue == 'Female') {
                userService.setGender('Female');
              }
              if(selectlist.answervalue == 'Male') {
                userService.setGender('Male');
              }

            } else {
              selectlist.value = false;
            }
          });
        }
      });
    };
    viewModel.isFemaleGender = function() {
      return userService.getGender() == 'Female';
    };
    // toggle selection for a given employee by name
    viewModel.toggleSelection = function toggleSelection(group, value) {
      angular.forEach(viewModel.riskFactors, function (list) {
        if (list.type == 'check') {
          if (group == 'cancertype') {
            var cancercount = 0,
              cancerinternalindex = 0;
            if (value == 'None') {
              for (var index = 0; index < list.selected.length - 1; index++) {
                if (list.selected[5].value_5) {
                  list.selected[index]['value_' + index] = false;
                }
              }
            }
            angular.forEach(list.selected, function (selectlist) {
              if (!selectlist['value_' + cancerinternalindex] && selectlist.answer != 'None') {
                cancercount++;
              }
              cancerinternalindex++;
            });
            if (cancercount == list.selected.length - 1) {
              var internindex = 0;
              angular.forEach(list.selected, function (selectlist) {
                if (selectlist.answer == 'None') {
                  selectlist['value_' + internindex] = true;
                  list.final = selectlist.answervalue;
                } else {
                  selectlist['value_' + internindex] = false;
                }
                internindex++;
              });
            } else if (cancercount < list.selected.length - 1) {
              var intindex = 0;
              angular.forEach(list.selected, function (selectlist) {
                if (selectlist.answer == 'None') {
                  selectlist['value_' + intindex] = false;
                } else {
                  list.final = selectlist.answervalue;
                }
                intindex++;
              });
            }
          } else if (group == 'food') {
            var foodcount = 0,
              foodinternalindex = 0;
            angular.forEach(list.selected, function (selectlist) {
              if (selectlist['value_' + foodinternalindex]) {
                foodcount = foodcount + selectlist.answervalue;
              }
              foodinternalindex++;
            });
            if (foodcount == 0) {
              list.final = 0;
            } else {
              list.final = foodcount;
            }
          }
        }
      });

      angular.forEach(viewModel.symptoms, function (symptomlist) {
        if (group == 'symptoms') {
          var countvalue = 0,
            indexvalue = 0;
          angular.forEach(symptomlist.selected, function (selectlist) {
            if (selectlist.answer == value) {
              
              angular.forEach(selectlist.symptoms, function (symptomvalue) {
                if (symptomvalue['value_' + indexvalue]) {
                  countvalue++;
                }
                indexvalue++;
              });
              if (countvalue == 0) {
                symptomlist.final = 0;
                userService.setOrgan('any type of');
              } else if (countvalue > 0 && countvalue <= 2) {
                symptomlist.final = 1;
                userService.setOrgan(value);
              } else if (countvalue > 2 && countvalue <= selectlist.symptoms.length - 1) {
                symptomlist.final = 2;
                userService.setOrgan(value);
              } else if (countvalue == selectlist.symptoms.length) {
                symptomlist.final = 3;
                userService.setOrgan(value);
              }
              userService.setSymptomsCount(countvalue);
            } else {
              var symptomindex = 0;
              angular.forEach(selectlist.symptoms, function (symptomvalue) {
                symptomvalue['value_' + symptomindex] = false;
                symptomindex++;
              });
            }
          });
        }
      });
    };
    viewModel.setTab = function (newTab) {
      viewModel.organtab = newTab;
      if(newTab == 'Prostate') {
        if(this.isFemaleGender()) {
          userService.showAlert({
          title: "Information !!!",
          details: "As you are female, it is not possible to develop prostate cancer in female so kindly select other organ.",
          className: "text-info"
        });  
        }
      }
      if (userService.getSymptomsCount() != 0)
      {
        userService.showAlert({
          title: "Information !!!",
          details: "If you choose another organ's symptoms then previous selection get diselect",
          className: "text-info"
        });
        userService.clearSymptomsCount();
      }
    };
    viewModel.isSet = function (tabName) {
      return viewModel.organtab === tabName;
    };
    /* for switching fieldset form  */
    viewModel.personalInformationForm = function () {
      window.scrollTo(0, 0);
      viewModel.stepForm = 'Personal Information';
      viewModel.percentage = {
        'width': '25%'
      };
    };
    viewModel.lifeStyleForm = function () {
      window.scrollTo(0,0);
      viewModel.stepForm = 'Life Style';
      viewModel.percentage = {
        'width': '50%'
      };
      angular.forEach(viewModel.riskFactors, function (riskvalues) {
        if (riskvalues.tab == 'Personal Information') {
          var newvalues = true;
          var riskkey = riskvalues.group;
          var riskvalue = riskvalues.final;
          userService.setArrays('associate', 'riskcount', riskkey, riskvalue, newvalues);
        }
      });
    };

    viewModel.eatingHabitForm = function () {
      window.scrollTo(0, 0);
      viewModel.stepForm = 'Eating Habit';
      viewModel.percentage = {
        'width': '75%'
      };
      angular.forEach(viewModel.riskFactors, function (riskvalues) {
        if (riskvalues.tab == 'Life Style') {
          var newvalues = true;
          var riskkey = riskvalues.group;
          var riskvalue = riskvalues.final;
          
          userService.setArrays('associate', 'riskcount', riskkey, riskvalue, newvalues);
        }
      });
    };
    viewModel.symptomsForm = function () {
      window.scrollTo(0, 0);
      viewModel.stepForm = 'Symptoms';
      viewModel.percentage = {
        'width': '100%'
      };
      angular.forEach(viewModel.riskFactors, function (riskvalues) {
        if (riskvalues.tab == 'Eating Habit') {
          var newvalues = true;
          var riskkey = riskvalues.group;
          var riskvalue = riskvalues.final;
          userService.setArrays('associate', 'riskcount', riskkey, riskvalue, newvalues);
        }
      });
    };

    viewModel.resultPage = function () {
      angular.forEach(viewModel.symptoms, function (riskvalues) {
        if (riskvalues.tab == 'Symptoms') {
          var newvalues = true;
          var riskkey = riskvalues.group;
          var riskvalue = riskvalues.final;
          if (riskvalue == 0) {
            userService.setOrgan("any type of");
            angular.element('#alert-message').html('Are You Sure you want to continue without selecting any symptoms!!');
            angular.element('#modal-alert-box').modal('show');
          } else {
            setTimeout(function () {
              $state.go('tester_dashboard.result');
            }, 1000);
          }
          userService.setArrays('associate', 'riskcount', riskkey, riskvalue, newvalues);
        }
      });
    };
    viewModel.goToResultPage = function() {
      var modalInstance = angular.element('#modal-alert-box').modal('hide');
      console.log(modalInstance);
      setTimeout(function() {
        $state.go('tester_dashboard.result');
      }, 500);
     
    };
  })
  .controller('mainController', function ($scope, $state, $rootScope, DataService, userService, $firebaseObject, testerDetails) {
    var vm = this;
    this.menulinks = 'menus/public_nav.html';
    var config = testerDetails.configDetails();

    var login = angular.element("#modal-login");
    var register = angular.element("#modal-register");

    this.loginModal = function () {
      login.modal("show");
    };
 
    this.registerModal = function () {
      register.modal("show");
    };

    this.checkLogin = function (users) {
      login.modal("hide");
      loginUser(users);

      function loginUser(users) {
        if (users.username != "" && users.password != "") {
          config.authRef.signInWithEmailAndPassword(users.username, users.password)
            .then(function (userObject) {
              var uid = config.authRef.currentUser.uid;
              config.userRef.child(uid).on('value', function (data) {
                var userObj = {
                  username: data.val().name,
                  role: data.val().role,
                  id: uid
                };
                userService.saveLoginData(userObj);
                if (userObj.role == 'admin') {
                  $state.go('admin_dashboard');
                } else {
                  $state.go('tester_dashboard.summary');
                }
              });

            })
            .catch(function (error) {
              result = 'failed';
              userService.showAlert({
                title: error.code,
                details: error.message,
                className: 'text-danger'
              });
            });
        } else {
          result = 'failed';
          userService.showAlert({
            title: "fields required error",
            details: "All fields are compulsory",
            className: "text-danger"
          });
        }
      }
    };

    this.resetRegister = function (user) {
      user.name = '';
      user.email = '';
      user.password = '';
      user.confirmPassword = '';

      $scope.formRegister.$setPristine();
    };
    this.checkRegister = function (users) {
      register.modal("hide");

      registerUser(users);

      function registerUser(users) {
        var currentuser = {
          role: "user",
          name: users.name,
          email: users.email
        };
        if (users.email != "" && users.password != "" && users.confirmPassword != "") {
          if (users.password == users.confirmPassword) {
            config.authRef.createUserWithEmailAndPassword(users.email, users.password)
              .then(function (userObject) {
                config.userRef.child(userObject.user.uid).set(currentuser);
                userService.showAlert({
                  title: "Registration Success!!",
                  details: "User Registeration done successfully with Email ID = " + userObject.user.email,
                  className: "text-success"
                });

              })
              .catch(function (error) {
                result = 'failed';
                userService.showAlert({
                  title: error.code,
                  details: error.message,
                  className: 'text-danger'
                });
              });
          } else {
            result = 'failed';
            userService.showAlert({
              title: "password mismatch error",
              details: "Password didn't match",
              className: "text-danger"
            });

          }
        } else {
          result = 'failed';
          userService.showAlert({
            title: "fields required error",
            details: "All fields are compulsory",
            className: "text-danger"
          });
        }
      }
    };
  });