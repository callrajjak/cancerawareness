app.filter('replaceSpaceToUnderscore',function(){
  return function (value) {
    return (!value) ? '' : value.replace(/[^A-Z0-9]+/ig, '_');
  };
});