 app.service("userService", [
  "DataService",
  function (DataService) {
    var username, gender;
    var user_login_value;
    var menuLinks = "menus/public_nav.html";
    var loggedin = false;
    var organ;
    var selectionList = [];
    var maleorgancountslist = [];
    var femaleorgancountslist = [];
    var maleCount = 0,femaleCount = 0;
    var lowcount = 0,
      mediumcount = 0,
      highcount = 0,
      symptomsCount = 0;
    var connection = false;

    this.onlineConnection = function() {
      connection = true;
    };
    this.isConnection = function(){
      return connection;
    };
    this.offlineConnection = function(){
      connection = false;
    };
    this.getOrganList = function(){
      return ['Lung', 'Brain', 'Breast', 'Ovarian', 'Prostate', 'Oral', 'Stomach'];
    };
    this.setMenuLinks = function (value) {
      menuLinks = value;
    };
    this.getTotalGender = function(type){
      totalGender = {
        males: maleCount,
        females: femaleCount
      };
      return totalGender;
    };
    this.setTotalGender = function(count,type){
      if(type == 'Male'){
        maleCount = count;
      }
      else if(type == 'Female'){
        femaleCount = count;
      }
    };
    this.getResultAverage = function(){
      resultAvg = {
        low: lowcount,
        medium: mediumcount,
        high:highcount
      };
      return resultAvg;
    };
    this.setResultAverage = function(count,type){
      if(type == 'Low'){
        lowcount = count;
      }else if(type == 'Medium'){
        mediumcount = count;
      }else if(type == 'High'){
        highcount = count;
      }
    };
    this.clearSymptomsCount = function(){
      symptomsCount = 0;
    };
    this.getSymptomsCount = function() {
      return symptomsCount;
    };
    this.setSymptomsCount = function(count) {
      symptomsCount = count;
    };
    this.getMenuLinks = function () {
      return menuLinks;
    };
    this.setLoginText = function (value) {
      user_login_value = value;
    };
    this.getLoginText = function () {
      return user_login_value;
    };
    this.setName = function (name) {
      username = name;
    };
    this.getName = function () {
      return username;
    };
    this.setGender = function (genderValue) {
      gender = genderValue;
    };
    this.getGender = function () {
      return gender;
    };
    this.setOrgan = function (organName) {
      organ = organName;
    };
    this.getOrgan = function () {
      return organ;
    };
    this.isUserLoggedIn = function () {
      if (!!localStorage.getItem('login')) {
        loggedin = true;
        var data = JSON.parse(localStorage.getItem('login'));
        username = data.username;
        id = data.uid;
      }
      return loggedin;
    };
    this.saveLoginData = function (loginData) {
      username = loginData.username;
      id = loginData.id;
      loggedin = true;
      localStorage.setItem('loginData', JSON.stringify({
        username: username,
        uid: id
      }));
    };
    this.userLoggedOut = function () {
      loggedin = false;
    };
    this.getRiskValues = function () {
      return selectionList;
    };
    this.clearRiskValues = function() {
      selectionList = [];
    };
    this.getMalesArray = function () {
      return maleorgancountslist;
    };
    this.getFemalesArray = function () {
      return femaleorgancountslist;
    };
    this.getArrays = function (arraytype, listtype, organlist) {
      var returnarray = [];
      if (arraytype == 'associate') {
        if (listtype == 'riskcount') {
          returnarray = selectionList;
        } else if (listtype == 'maleorgancount') {
          var males = [];
          angular.forEach(organlist, function (organ) {
            var malefound = false;
            angular.forEach(maleorgancountslist, function (maleorgan) {
              if (maleorgan.hasOwnProperty(organ)) {
                malefound = true;
              }
            });
            if (malefound) {
              angular.forEach(maleorgancountslist, function (maleorgan) {
                if (maleorgan.hasOwnProperty(organ)) {
                  males.push(maleorgan[organ]);
                }
              });
            } else {
              males.push(0);
            }
            returnarray = males;
          });
        } else if (listtype == 'femaleorgancount') {
          var females = [];
          angular.forEach(organlist, function (organ) {
            var femalefound = false;
            angular.forEach(femaleorgancountslist, function (femaleorgan) {
              if (femaleorgan.hasOwnProperty(organ)) {
                femalefound = true;
              }
            });
            if (femalefound) {
              angular.forEach(femaleorgancountslist, function (femaleorgan) {
                if (femaleorgan.hasOwnProperty(organ)) {
                  females.push(femaleorgan[organ]);
                }
              });
            } else {
              females.push(0);
            }
          });
          returnarray = females;
        }
        return returnarray;
      }
    };
    this.getIndexArray = function () {
      return ratio;
    };
    this.setArrays = function (arraytype, listtype, key, value, newvalue) {
      if (arraytype == 'associate') {
        if (listtype == 'maleorgancount') {
          if (maleorgancountslist.length != 0) {
            angular.forEach(maleorgancountslist, function (listkey) {
              if (listkey.hasOwnProperty(key)) {
                newvalue = false;
              }
            });
          }
          if (newvalue) {
            maleorgancountslist.push({
              [key]: value
            });
          } else {
            angular.forEach(maleorgancountslist, function (maleorgankey) {
              if (maleorgankey.hasOwnProperty(key)) {
                maleorgankey[key] = value;
              }
            });
          }
        } else if (listtype == 'femaleorgancount') {
          if (femaleorgancountslist.length != 0) {
            angular.forEach(femaleorgancountslist, function (listkey) {
              if (listkey.hasOwnProperty(key)) {
                newvalues = false;
              }
            });
          }
          if (newvalue) {
            femaleorgancountslist.push({
              [key]: value
            });
          } else {
            angular.forEach(femaleorgancountslist, function (femaleorgankey) {
              if (femaleorgankey.hasOwnProperty(key)) {
                femaleorgankey[key] = value;
              }
            });
          }
        } else if (listtype == 'riskcount') {
          if (selectionList.length != 0) {
            angular.forEach(selectionList, function (listkey) {
              if (listkey.hasOwnProperty(key)) {
                newvalues = false;
              }
            });
          }
          if (newvalue) {
            selectionList.push({
              [key]: value
            });
          } else {
            angular.forEach(selectionList, function (riskkey) {
              if (riskkey.hasOwnProperty(key)) {
                riskkey[key] = value;
              }
            });
          }
        }
      }
    };

    this.showAlert = function (opts) {
      var title = opts.title;
      var details = opts.details;
      className = "modal-body " + opts.className;
      angular.element("#message_title").text(title);
      angular
        .element("#message_details")
        .text(details)
        .removeClass()
        .addClass(className);
      angular.element("#message_box").modal("show");
    };

    this.generateRiskFactorsList = function () {
      var list = DataService.riskFactorsData;
      angular.forEach(list, function (value) {
        value.selected = [];
        if (value.type == "check") {
          var internalindex = 0;
          angular.forEach(value.possibilities, function (symptomvalues) {
            var checkvalue = "value_" + internalindex;
            value.selected.push({
              answer: symptomvalues.answer,
              answervalue: symptomvalues.answervalue,
              [checkvalue]: symptomvalues.value
            });
            if (symptomvalues.value == true) {
              value.final = symptomvalues.answervalue;
            }
            internalindex++;
          });
        }
        if (value.type == "radio") {
          angular.forEach(value.possibilities, function (symptomvalues) {
            value.selected.push({
              answer: symptomvalues.answer,
              answervalue: symptomvalues.answervalue,
              value: symptomvalues.value
            });
            if (symptomvalues.value == true) {
              value.final = symptomvalues.answervalue;
            }
          });
        }
      });
      return list;
    };
    function generatesymptoms(){
      var list = DataService.symptomsData;
      angular.forEach(list, function (listvalue) {
        var listindex = 0;
        listvalue.selected = [];
        angular.forEach(listvalue.possibilities, function (possiblevalue) {
          listvalue.selected.push({
            answer: possiblevalue.answer,
            symptoms: []
          });
          for (var i = 0; i < possiblevalue.symptoms.length; i++) {
            listvalue.selected[listindex].symptoms.push({
              answer: possiblevalue.symptoms[i].answer,
              answervalue: possiblevalue.symptoms[i].answervalue,
              ["value_" + i]: possiblevalue.symptoms[i].value
            });
          }
          listindex++;
        });
      });
      return list;
    }
    this.generateSymptomsList = function () {
      return generatesymptoms();
    };
  }
]);