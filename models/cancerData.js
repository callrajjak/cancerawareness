app.factory("DataService",['$http', function ($http) {
  
    var cancerDataObj = {
      riskFactorsData: riskFactorsData('riskfactor'),
      symptomsData: riskFactorsData('symptoms')
    };
    return cancerDataObj;
  
    function riskFactorsData(typeData) {
      var selectionList = [];
      $http.get('data/CASData.json')
      .then(function (response) {
        var list = response.data[0].sampleQuestions;
        if (typeData == 'riskfactor') {
          angular.forEach(list, function (value) {
            if (value.tab != 'Symptoms') {
              selectionList.push(value);
            }
          });
        }
        else 
        {
          angular.forEach(list, function (value) {
            if (value.tab == 'Symptoms') {
              selectionList.push(value);
            }
          });
        }
      });
      return selectionList;
    }
  } ]);