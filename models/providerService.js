app.config([
  "testerDetailsProvider",
  function (testerDetailsProvider) {
    // Initialize Firebase
    var firebaseconfig = {
      
      apiKey: "AIzaSyCOysY6YqaxheVPv3eiWXL4flKLbHYK8SA",
      authDomain: "cancerawareness-a981f.firebaseapp.com",
      databaseURL: "https://cancerawareness-a981f.firebaseio.com",
      projectId: "cancerawareness-a981f",
      storageBucket: "cancerawareness-a981f.appspot.com",
      messagingSenderId: "342600846007"

    };

    testerDetailsProvider.config(firebaseconfig);
  }
]);

app.provider("testerDetails", function () {
  /* variable declaration */

  var db = "";
  var auth = "";

  /* configure variables */
  this.config = function (config) {
    firebase.initializeApp(config);
    db = firebase.database().ref();
    auth = firebase.auth();

  };

  this.$get = ["$state", "$log", "DataService", "$q","userService", function ($state, $log, DataService, $q, userService) {
    var testerService = {};

    var user = db.child("users");
    var deferred = $q.defer();

    testerService.configDetails = function () {
      confDetails = {
        dbRef: db,
        userRef: user,
        authRef: auth
      };
      return confDetails;
    };
    
    /* if(navigator.onLine){
      alert("connected");
      userService.onlineConnection();
    }
    else
    {
      alert("not connected");
      userService.offlineConnection();
    } */

    var maleuser = '';
    var femaleuser = '';
    var organlist = [];
    organlist = userService.getOrganList();
   
    if (organlist.length != 0) {
      angular.forEach(organlist, function (organ) {
        var count = 0;
        var lowcount = 0,
          mediumcount = 0,
          highcount = 0;
        var newvalue = true;
        user.orderByChild('gender').equalTo('Male').on("value", function (snap) {
          maleuser = snap.val();
          userService.setTotalGender(snap.numChildren(),'Male');
          if (maleuser) {
            angular.forEach(maleuser, function (childValue) {
              results = childValue.result;
              angular.forEach(results, function (resultValue) {
                testorgan = resultValue.testorgan;
                if (organ == testorgan) {
                  count++;
                  userService.setArrays('associate', 'maleorgancount', organ, count, newvalue);
                }
                testresult = resultValue.testresult;
                if(testresult == 'Low'){
                  userService.setResultAverage(++lowcount,'Low');
                }
                if(testresult == 'Medium'){
                  userService.setResultAverage(++mediumcount,'Medium');
                }
                if(testresult == 'High'){
                  userService.setResultAverage(++highcount,'High');
                }
              });
            });
          }
        });
        user.orderByChild('gender').equalTo('Female').once("value", function (snap) {
          femaleuser = snap.val();
          userService.setTotalGender(snap.numChildren(), 'Female');
          if (femaleuser) {
            angular.forEach(femaleuser, function (childValue) {
              results = childValue.result;
              angular.forEach(results, function (resultValue) {
                testorgan = resultValue.testorgan;
                if (organ == testorgan) {
                  count++;
                  userService.setArrays('associate', 'femaleorgancount', organ, count, newvalue);
                }
                testresult = resultValue.testresult;
                if (testresult == 'Low') {
                  userService.setResultAverage(++lowcount, 'Low');
                }
                if (testresult == 'Medium') {
                  userService.setResultAverage(++mediumcount, 'Medium');
                }
                if (testresult == 'High') {
                  userService.setResultAverage(++highcount, 'High');
                }
              });
            });
          }
        });
      });
    }
    testerService.getMaleRatioDetails = function () {
      return maleuser;
    };
    testerService.getFemaleRatioDetails = function () {
      return femaleuser;
    };
    testerService.updateTesterDetails = function (value) {
      user
        .child("user")
        .update({
          location: value
        })
        .catch(function (err) {
          console.log("one of this update failed", err);
        });
    };
    testerService.removeTesterDetails = function () {
      return user.child("second text").remove();
    };

    return testerService;
  }];
});